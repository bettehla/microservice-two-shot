# Wardrobify

  Need to keep track of your shoes and hats? We have the solution for you!

## Team

- Bette - Shoes microservice
- Jared - Hats microservice

## How to run the project

1. Install Docker if you do not already have it installed ([Official Docker website](https://www.docker.com/))
2. Clone this repository to your local machine
3. CD into the project directory and run the following commands from your terminal:

```
docker volume create pgdata
docker-compose build
docker-compose up
```

4. You will need to create a few locations and/or bins via an API client such as [Insomnia](https://insomnia.rest/) using the API information provided below
5. No hats or shoes will have been created in the database, but you can do so via an API client using the API information provided below.  Please note that you need to create at least one location and/or bin in step 3 above, in order to be able to create hats and/or shoes.
6. You will be able to access the project front-end at http://localhost:3000/.  First, you need to access the terminal in both the hats api and shoes api containers and run the following command to create a superuser account in each:

```
python manage.py createsuperuser
```

## Design diagram

![wardrobify-design-diagram](./docs/wardrobify-design-diagram.png)

## Wardrobe

### Locations model

| Fields         | Type                      | Attributes     |
| -------------- | ------------------------- | -------------- |
| closet_name    | CharField                 | max_length=100 |
| section_number | PositiveSmallIntegerField |                |
| shelf_number   | PositiveSmallIntegerField |                |

### Bins model

| Fields      | Type                      | Attributes     |
| ----------- | ------------------------- | -------------- |
| closet_name | CharField                 | max_length=100 |
| bin_number  | PositiveSmallIntegerField |                |
| bin_size    | PositiveSmallIntegerField |                |

### REST API

| Method | URL                                      | Description               |
| ------ | ---------------------------------------- | ------------------------- |
| GET    | http://localhost:8100/api/locations/     | Get a list of locations   |
| GET    | http://localhost:8100/api/locations/:id/ | Get details of a location |
| POST   | http://localhost:8100/api/locations/     | Create a location         |
| DELETE | http://localhost:8100/api/locations/:id/ | Delete a location         |
| GET    | http://localhost:8100/api/bins/          | Get a list of bins        |
| GET    | http://localhost:8100/api/bins/:id/      | Get details of a bin      |
| POST   | http://localhost:8100/api/bins/          | Create a bin              |
| DELETE | http://localhost:8100/api/bin/:id/       | Delete a bin              |

## Hats

The Hats microservice integrates with the Wardrobe API to track hats in your wardrobe, including their location in your closets.  This microservice makes an immutable copy of all locations in the form of LocationsVO, by polling the Wardrobe API for added locations on a regular basis (currently every 60 seconds).

Please note:
When a location is deleted from the Wardrobe API, all hats linked to that location are also deleted.

### locationVO model (value object)

| Fields         | Type                      | Attributes                  |
| -------------- | ------------------------- | --------------------------- |
| closet_name    | CharField                 | max_length=100              |
| section_number | PositiveSmallIntegerField |                             |
| shelf_number   | PositiveSmallIntegerField |                             |
| import_href    | CharField                 | max_length=200, unique=True |

### Hat model

| Fields    | Type                    | Attributes                                   |
| --------- | ----------------------- | -------------------------------------------- |
| style     | CharField               | max_length=200                               |
| color     | CharField               | max_length=200                               |
| fabric    | CharField               | max_length=200                               |
| photo_url | URLField                |                                              |
| created   | DateTimeField           | auto_now_add=True                            |
| location  | ForeignKey (LocationVO) | related_name="hat", on_delete=models.CASCADE |

### REST API

| Method | URL                                 | Description           |
| ------ | ----------------------------------- | --------------------- |
| GET    | http://localhost:8090/api/hats/     | Get a list of hats    |
| GET    | http://localhost:8090/api/hats/:id/ | Get details for a hat |
| POST   | http://localhost:8090/api/hats/     | Create a hat          |
| DELETE | http://localhost:8090/api/hats/:id/ | Delete a hat          |

## Shoes

Similar to Hats microservice, Shoes microservice track shoes in your wardrobe using BinVO. We use API views and utilize RESTful routes to allow adding, deleteing, updating, and viewing instances of Shoe that is affiliated with a BinVO property. BinVO instances are updated or created each time in the wardrobe microservice in poller.py.

Bin Value Object

Bin information is in the Wardrobe microservce, we use it for the shoe microservice. We would get a copy of all bin data so that BinVO is created to polling the information periodically.

### binVO model (value object)

| Fields      | Type                      | Attributes                  |
| ----------- | ------------------------- | --------------------------- |
| closet_name | CharField                 | max_length=100              |
| bin_number  | PositiveSmallIntegerField |                             |
| bin_size    | PositiveSmallIntegerField |                             |
| import_href | CharField                 | max_length=200, unique=True |

### Shoe model

| Fields       | Type               | Attributes                                     |
| ------------ | ------------------ | ---------------------------------------------- |
| model_name   | CharField          | max_length=200                                 |
| manufacturer | CharField          | max_length=200                                 |
| color        | CharField          | max_length=100                                 |
| picture_url  | URLField           | null=true                                      |
| bin          | ForeignKey (BinVO) | related_name="shoes", on_delete=models.CASCADE |

### REST API

| Method | URL                                  | Description                     |
| ------ | ------------------------------------ | ------------------------------- |
| GET    | http://localhost:8080/api/shoes/     | Get a list of shoes             |
| GET    | http://localhost:8080/api/shoes/:id/ | Get details for a pair of shoes |
| POST   | http://localhost:8080/api/shoes/     | Create a pair of shoes          |
| DELETE | http://localhost:8080/api/shoes/:id/ | Delete a pair of shoes                   |
