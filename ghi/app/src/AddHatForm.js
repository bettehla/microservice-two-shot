import React from "react";
import { useNavigate } from "react-router-dom"

function withExtras(Component) {
    return (props) => (
        <Component {...props} useNavigate={useNavigate()} />
    );
}

class AddHatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            style: "",
            color: "",
            fabric: "",
            photoUrl: "",
            location: "",
            locations: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeStyle = this.handleChangeStyle.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeFabric = this.handleChangeFabric.bind(this);
        this.handleChangePhotoUrl = this.handleChangePhotoUrl.bind(this);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.photo_url = data.photoUrl;
        delete data.photoUrl;
        delete data.locations;

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            this.setState({
                style: '',
                color: '',
                fabric: '',
                photoUrl: '',
                location: '',
            });
            this.props.useNavigate("/hats")
        }
    }

    handleChangeStyle(event) {
        const value = event.target.value;
        this.setState({ style: value });
    }

    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleChangeFabric(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
    }

    handleChangePhotoUrl(event) {
        const value = event.target.value;
        this.setState({ photoUrl: value });
    }

    handleChangeLocation(event) {
        const value = event.target.value;
        this.setState({ location: value });
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeStyle} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                                <label htmlFor="style">Style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeFabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangePhotoUrl} placeholder="Photo URL" required type="text" name="photoUrl" id="photoUrl" className="form-control" />
                                <label htmlFor="photoUrl">Photo URL</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChangeLocation} value={this.state.location} required name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>{location.closet_name}, Section {location.section_number}, Shelf {location.shelf_number}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default withExtras(AddHatForm);
