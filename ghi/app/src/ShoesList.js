import React from "react"


function ShoesList(props) {
    const deleteShoe = async (id) => {
        const url = `http://localhost:8080/api/shoes/${id}/`
        const options = {
            method: "DELETE"
        }
        const response = await fetch(url, options)

        window.location.reload();
    }

    {

        return (
            <table className="table table-striped align-middle mt-5">
                <thead>
                    <tr>
                        <th>Shoe</th>
                        <th>Color</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map((shoe) => {
                        console.log(shoe)
                        return (
                            <tr key={shoe.id}>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.manufacturer}</td>
                                <td><img src={shoe.picture_url} className="img-thumbnail shoes" alt={shoe.manufacturer}></img></td>
                                <td>{shoe.bin}</td>
                                <td>
                                    <button type="button" value={shoe.id} onClick={() => deleteShoe(shoe.id)}>Delete </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>

        );
    }
}
export default ShoesList;
